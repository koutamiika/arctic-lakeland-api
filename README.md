# Arctic Lakeland API -lisäosa

Tämä lisäosa luo ***all_product*** artikkelityypin, jonka artikkelit listataan REST API:ssa. Lisäosa tarjoaa myös työkalun, jolla olemassa olevia tuotteita voidaan muuttaa Arctic Lakeland tuotekorteiksi.

## Endpointit

    GET /all/v1/products // Palauttaa kaikki tuotteet.
    GET /all/v1/products/<ID> // Palauttaa yksittäisen tuotteen.
    GET /all/v1/category/<slug> // Palauttaa tuotteet kategorian perusteella.

Palauttaa Product objectin.

    {
	    "ID": 6172,
	    "title": "Minun tuote",
	    "company_name": "Ab Härpäke Oy",
	    "email": "info@minunyritys.fi",
	    "phone": "044 123 4567",
	    "homepage": "https://www.minunyritys.fi",
	    "description": "lorem ipsum dolor set amet....",
	    "short_description": "Tässä tuotteen lyhty kuvaus",
	    "price": "123",
	    "price_suffix": "/ 2yö / 2hlö",
	    "thumbnail": "https://www.minunyritys.fi/wp-content/uploads/2017/06/kuva-1.jpg",
	    "images": [
		    "https://www.minunyritys.fi/wp-content/uploads/2017/06/kuva-2.jpg",
		    "https://www.minunyritys.fi/wp-content/uploads/2020/12/kuva-3.jpg",
	    ],
	    "category": [
		    "Kulttuuri"
	    ],
	    "booking": "",
	    "location": {
		    "street": "Kauppakatu, Kajaani, Suomi",
		    "lat": "64.224245",
		    "lon": "27.734437100000036"
	    },
	    "post_date": "2021-05-16 08:12:54",
	    "post_modified": "2021-05-16 08:12:54",
	    "lang": "fi",
		"url": "https://sivustonosoite.fi/minun-tuote",
		"offer_enddate": "2023-01-10"
    }

### Attribuutit
- **ID:** *(integer)* Artikkelin yksilöllinen ID.
- **title:** *(string)* Tuotteen otsikko.
- **company_name:** *(string)* Yrityksen nimi.
- **email:** *(string)* Yrityksen sähköpostiosoite.
- **phone:** *(string*) Yrityksen puhelinnumero.
- **description:** *(string)* Pitkä kuvaus tuotteesta / palvelusta.
- **short_description:** *(string)* Lyhyt kuvaus tuotteesta / palvelusta.
- **price:** *(float)* Tuotteen hinta. Vain numero.
- **price_suffix:** *(string)* Teksti, joka näytetään hinnan jälkeen. Esim. / 2vrk / 2hlö tai per hlö.
- **thumbnail:** *(string)* Artikkelin pääkuva.
- **images:** (array/string) Tuotteen / palvelun mahdolliset lisäkuvat.
- **category:** *(array/string)* Tuotteen kategoria.
- **booking:** (string) Linkki mahdolliseen varausjärjestelmään.
- **location:** *(array/string)* Tuotteen / palvelun / yrityksen sijainti kartalla.
- **post_date:** *(string)* Artikkelin julkaisupäivämäärä.
- **post_modified:** *(string)* Aika, jolloin artikkelia on muokattu viimeksi.
- **lang:** *(string)* Artikkelin kieli.
- **url:** *(string)* Linkki tuotteeseen matkailualueen sivustolla (permalink).
- **offer_enddate:** (string) Tarjoustuotteen kampanjan päättymispäivä (muodossa Y-m-d).

## Parametrit
REST API Endpoint hyväksyy parametrejä, joilla tuloksia voidaan suodattaa.

Esim:

    GET /all/v1/products?per_page=50&order=ASC

Käytettävissä olevat parametrit ovat:

- **per_page:** Kuinka monta tulosta näytetään. Oletuksena 10. "-1" palauttaa kaikki.
- **order:** Tulosten järjestys. ASC: nouseva järjestys, DESC: laskeva järjestys.
- **orderby:** Minkä parametrin perustella tulokset järjestetään. Oletuksena "date". Käytettävissä olevat parametrit löytyy https://developer.wordpress.org/reference/classes/wp_query/#order-orderby-parameters
- **lang:** Tulokset kielen perusteella. Arvona kielen Polylang slug. Esim. "fi" tai "en".
- **page:** Tulosten tietty sivu, jos *per_page* ei ole -1.
- **search:** Vapaa sanahaku.

## Actions
Lista actioneista, jotka lisäosa tarjoaa.

| Action                 | Args                                                                                                           | Description                                  |
|------------------------|----------------------------------------------------------------------------------------------------------------|----------------------------------------------|
| before_save_categories | $post = Post Object,<br>$new_post_id = (int) ID of the duplicated post,<br>$cats = (array) Array of categories | Runs before taxonomies are saved to new post |

## Filters
Lista filtereistä, jotka lisäosa tarjoaa.

| Filter                     | Args                                               | Description                                                    |
|----------------------------|----------------------------------------------------|----------------------------------------------------------------|
| all_insert_post_args       | $args = Array of arguments,<br>$post = Post object | Arguments for wp_insert_post() when creating a duplicate post. |
| all_allowed_post_types     | $post_type = array of post types                   | Post type to allow converter tool for.                         |
