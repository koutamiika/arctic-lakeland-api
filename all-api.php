<?php
/**
 * The plugin bootstrap file
 *
 * @link              https://koutamedia.fi
 * @since             0.1.0
 * @package           All_Api
 *
 * @wordpress-plugin
 * Plugin Name:       Arctic Lakeland API
 * Description:       Provides REST API Endpoint for Arctic Lakeland.
 * Version:           1.0.1
 * Author:            Kouta Media
 * Author URI:        https://koutamedia.fi
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       all-api
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 */
define( 'ALL_API_VERSION', '1.0.1' );

/**
 * Plugin base dir path.
 * used to locate plugin resources primarily code files
 */
define( 'ALL_API_BASE_DIR', plugin_dir_path( __FILE__ ) );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-all-api-activator.php
 */
function activate_all_api() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-all-api-activator.php';
	All_Api_Activator::activate();
}

register_activation_hook( __FILE__, 'activate_all_api' );

/**
 * The core plugin class.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-all-api.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    0.1.0
 */
function run_all_api() {

	$plugin = new All_Api();
	$plugin->run();

}
run_all_api();
