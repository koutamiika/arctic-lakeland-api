<?php
/**
 * Define the products functionality
 *
 * @link       https://koutamedia.fi
 * @since      0.1.0
 *
 * @package    All_Api
 * @subpackage All_Api/includes
 */

/**
 * Define the products functionality.
 * *
 * @since      0.1.0
 * @package    All_Api
 * @subpackage All_Api/includes
 * @author     Kouta Media / Miika Salo <miika@koutamedia.fi>
 */
class All_Api_Products {

	/**
	 * The ID of this plugin.
	 *
	 * @since    0.1.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    0.1.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Plugin options
	 *
	 * @var array
	 */
	public $options;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    0.1.0
	 * @param      string $plugin_name       The name of this plugin.
	 * @param      string $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version     = $version;
		$this->options     = get_option( 'all-api-settings' );

	}

    /**
     * Meta box initialization.
     */
    public function init_metabox() {
        add_action( 'add_meta_boxes', array( $this, 'add_metabox' ) );
        add_action( 'save_post', array( $this, 'save_metabox' ), 10, 2 );
    }

    /**
     * Adds the meta box.
     */
    public function add_metabox() {

        add_meta_box(
            'all-company-info',
            __( 'Yrityksen tiedot', 'all-api' ),
            array( $this, 'render_company_info_metabox' ),
            'all_product',
            'normal',
            'default'
        );

        add_meta_box(
            'all-short-description',
            __( 'Lyhyt kuvaus', 'all-api' ),
            array( $this, 'render_short_description_meta' ),
            'all_product',
            'normal',
            'default'
        );

        add_meta_box(
            'all-location',
            __( 'Sijainti', 'all-api' ),
            array( $this, 'render_location_meta' ),
            'all_product',
            'normal',
            'default'
        );

        add_meta_box(
            'all-gallery',
            __( 'Kuvagalleria', 'all-api' ),
            array( $this, 'render_gallery_meta' ),
            'all_product',
            'normal',
            'default'
        );

		if ( current_user_can( 'publish_pages' ) ) {
			add_meta_box(
				'all-product-sync',
				__( 'Arctic Lakeland Synkronointi', 'all-api' ),
				array( $this, 'render_sync_meta' ),
				'all_product',
				'side',
				'default'
			);

			add_meta_box(
				'all-product-card',
				__( 'Arctic Lakeland tuotekortti', 'all-api' ),
				array( $this, 'render_product_card_sync_meta' ),
				apply_filters( 'all_allowed_post_types', $this->options['post_type'] ),
				'side',
				'default'
			);
		}

        add_meta_box(
            'all-product-season',
            __( 'Kausi', 'all-api' ),
            array( $this, 'render_season_meta' ),
            'all_product',
            'side',
            'default'
        );
    }

    /**
     * Renders the meta box.
     */
    public function render_season_meta( $post ) {

		$seasons = array(
			__( 'Kesä', 'all-api' ),
			__( 'Talvi', 'all-api' ),
		);

		$current_seasons = get_post_meta( $post->ID, 'all_product_season', true ) ?: array();

		if ( $seasons ) {

			echo '<ul>';

			foreach ( $seasons as $key => $value ) {
				echo '<li>';
				echo '<input type="checkbox" id="product_season_' . $key . '" name="all_product_season[]"' . checked( in_array( $value, $current_seasons, true ), true, false ) . ' value="' . esc_attr( $value ) . '" />';
				echo '<label for="product_season_' . esc_attr( $key ) . '">' . esc_html( $value ) . '</label>';
				echo '</li>';
			}

			echo '</ul>';
		}

    }

    /**
     * Renders the meta box.
     */
    public function render_sync_meta( $post ) {

		$parent = get_post_meta( $post->ID, 'all_post_parent', true );
		$sync   = get_post_meta( $post->ID, 'all_product_sync', true );

		if ( $parent ) { ?>
			<p><?php printf( __( 'Tämä tuotekortti on luotu sisällöstä <a href="%1$s">%2$s</a>.', 'all-api' ), esc_url( get_edit_post_link( $parent ) ), esc_html( get_the_title( $parent ) ) ); ?></p>
			<hr>
			<p><strong>Synkronointi</strong></p>
			<label for="all-product-sync-checkbox">
				<input type="checkbox" name="all-product-sync-checkbox" id="all-product-sync-checkbox" data-id="<?php echo esc_attr( $post->ID ); ?>" <?php checked( $sync, 'sync' ); ?>>
				<?php echo esc_html__( 'Ota käyttöön sisällön synkronointi', 'all-api' ); ?>
			</label>
			<div class="messages"></div>
			<?php
		} else { ?>
			<p><?php echo esc_html__( 'Tätä tuotekorttia ei ole linkitetty olemassa olevaan sisältöön.', 'all-api' ); ?></p>
			<?php
		}

    }

    /**
     * Renders the meta box.
     */
    public function render_product_card_sync_meta( $post ) {

		$args = array(
			'post_type'      => 'all_product',
			'post_status'    => 'any',
			'posts_per_page' => 1,
			'meta_query'     => array( // phpcs:ignore WordPress.DB.SlowDBQuery.slow_db_query_meta_query
				array(
					'key'     => 'all_post_parent',
					'value'   => $post->ID,
					'compare' => '=',
				),
			),
		);

		$children = get_posts( $args );

		if ( $children ) { ?>

			<?php $sync = get_post_meta( $children[0]->ID, 'all_product_sync', true ); ?>

			<p><?php printf( __( 'Arctic Lakeland tuotekortti on luotu. Voit muokata sitä <a href="%1$s"> tästä</a>.', 'all-api' ), esc_url( get_edit_post_link( $children[0] ) ) ); ?></p>
			<hr>
			<p><strong>Synkronointi</strong></p>
			<label for="all-product-sync-checkbox">
				<input type="checkbox" name="all-product-sync-checkbox" id="all-product-sync-checkbox" data-id="<?php echo esc_attr( $children[0]->ID ); ?>" <?php checked( $sync, 'sync' ); ?>>
				<?php echo esc_html__( 'Ota käyttöön sisällön synkronointi', 'all-api' ); ?>
			</label>
			<div class="messages"></div>
			<?php
		} else { ?>
			<p><?php echo esc_html__( 'Arctic Lakeland tuotekorttia ei ole vielä luotu.', 'all-api' ); ?></p>
			<a href="<?php echo esc_url( wp_nonce_url( 'admin.php?action=all_duplicate_post&post=' . $post->ID, 'duplicate_nonce_action', 'duplicate_nonce' ) ); ?>" class="button">Luo tuotekortti</a>
			<?php
		}

    }

    /**
     * Renders the meta box.
     */
    public function render_gallery_meta( $post ) {

		echo '<p>Lisää kuvia kuvagalleriaan painamalla "Lisää kuvia" -painiketta.</p>'; // Update or remove me.

		$meta_key = 'all_gallery';
		echo $this->all_gallery_field( $meta_key, get_post_meta( $post->ID, $meta_key, true ) );

    }

	/**
	 * Render html for gallery meta box.
	 */
	public function all_gallery_field( $name, $value = '' ) {

		if ( is_array( $value ) ) {
			$value = null;
		}

		$html = '<div class="all-gallery-wrapper"><ul class="all_gallery_list">';
		/* array with image IDs for hidden field */
		$hidden = array();

		$args = array(
			'post_type'      => 'attachment',
			'orderby'        => 'post__in', /* we have to save the order */
			'order'          => 'ASC',
			'post__in'       => explode( ',', $value ), /* $value is the image IDs comma separated */
			'numberposts'    => -1,
			'post_mime_type' => 'image',
		);

		$images = get_posts( $args );

		if ( $images ) {

			foreach ( $images as $image ) {
				$hidden[]  = $image->ID;
				$image_src = wp_get_attachment_image_src( $image->ID, array( 80, 80 ) );
				$html     .= '<li data-id="' . $image->ID . '"><span style="background-image:url(' . esc_url( $image_src[0] ) . ')"></span><div class="actions"><a href="#" class="all_gallery_remove">×</a></div></li>';
			}
		}

		$html .= '</ul></div>';
		$html .= '<input type="hidden" name="' . $name . '" value="' . implode( ',', $hidden ) . '" /><a href="#" class="button all_upload_gallery_button">' . esc_html__( 'Lisää kuvia', 'all-api' ) . '</a>';

		return $html;
	}

    /**
     * Renders the meta box.
     */
    public function render_location_meta( $post ) {

		echo '<p>Hae sijainti osoitteella, tai raahaa karttamerkin kohtaa kartasta. Sijainti tallentuu vain, jos alla olevassa kentässä on osoite. Karttamerkki näkyy vaikka osoitetta ei ole syötetty. </p>'; // Update or remove me.

		$location = get_post_meta( $post->ID, 'all_product_location', true );

		echo '<div class="all-map-wrapper">';
		echo '<input type="text" id="address" name="all_location[street]" autocomplete="off" value="' . ( isset( $location['street'] ) ? esc_html( $location['street'] ) : '' ) . '"/>';
		echo '<input type="hidden" id="lat" name="all_location[lat]" value="' . ( isset( $location['lat'] ) ? esc_attr( $location['lat'] ) : '' ) . '"/>';
		echo '<input type="hidden" id="lon" name="all_location[lon]" value="' . ( isset( $location['lon'] ) ? esc_attr( $location['lon'] ) : '' ) . '"/>';
		echo '<div id="all-map"></div>';
		echo '</div>';

    }

    /**
     * Renders the meta box.
     */
    public function render_short_description_meta( $post ) {

		echo '<p>Lisää lyhyt kuvaus.</p>'; // Update or remove me.

		$data = get_post_meta( $post->ID, 'all_short_description', true );

		// Render WYSIWYG -editor.
		wp_editor(
			$data,
			'all_short_description_editor',
			array(
				'textarea_name' => 'all_short_description',
				'textarea_rows' => 8,
				'teeny'         => true,
				'media_buttons' => false,
			)
		);

    }

    /**
     * Renders the meta box.
     */
    public function render_company_info_metabox( $post ) {
        // Add nonce for security and authentication.
		wp_nonce_field( 'edit_products_nonce_action', 'edit_products_nonce' );

		include_once ALL_API_BASE_DIR . 'admin/partials/all-api-company-info-metafields.php';

    }

    /**
     * Handles saving the meta box.
     *
     * @param int     $post_id Post ID.
     * @param WP_Post $post    Post object.
     * @return null
     */
    public function save_metabox( $post_id, $post ) {
        // Add nonce for security and authentication.
        $nonce_name   = isset( $_POST['edit_products_nonce'] ) ? $_POST['edit_products_nonce'] : '';
        $nonce_action = 'edit_products_nonce_action';

        // Check if nonce is valid.
        if ( ! wp_verify_nonce( $nonce_name, $nonce_action ) ) {
            return;
        }

        // Check if user has permissions to save data.
        if ( ! current_user_can( 'edit_post', $post_id ) ) {
            return;
        }

        // Check if not an autosave.
        if ( wp_is_post_autosave( $post_id ) ) {
            return;
        }

        // Check if not a revision.
        if ( wp_is_post_revision( $post_id ) ) {
            return;
		}

		if ( isset( $_POST['all_company_name'] ) ) {
			update_post_meta( $post_id, 'all_company_name', sanitize_text_field( wp_unslash( $_POST['all_company_name'] ) ) );
		}

		if ( isset( $_POST['all_company_address'] ) ) {
			update_post_meta( $post_id, 'all_company_address', sanitize_text_field( wp_unslash( $_POST['all_company_address'] ) ) );
		}

		if ( isset( $_POST['all_company_email'] ) ) {
			update_post_meta( $post_id, 'all_company_email', sanitize_text_field( wp_unslash( $_POST['all_company_email'] ) ) );
		}

		if ( isset( $_POST['all_company_phone'] ) ) {
			update_post_meta( $post_id, 'all_company_phone', sanitize_text_field( wp_unslash( $_POST['all_company_phone'] ) ) );
		}

		if ( isset( $_POST['all_company_website'] ) ) {
			update_post_meta( $post_id, 'all_company_website', sanitize_text_field( wp_unslash( $_POST['all_company_website'] ) ) );
		}

		if ( isset( $_POST['all_company_booking'] ) ) {
			update_post_meta( $post_id, 'all_company_booking', sanitize_text_field( wp_unslash( $_POST['all_company_booking'] ) ) );
		}

		if ( isset( $_POST['all_product_price'] ) ) {
			update_post_meta( $post_id, 'all_product_price', sanitize_text_field( wp_unslash( $_POST['all_product_price'] ) ) );
		}

		if ( isset( $_POST['all_price_suffix'] ) ) {
			update_post_meta( $post_id, 'all_price_suffix', sanitize_text_field( wp_unslash( $_POST['all_price_suffix'] ) ) );
		}

		if ( isset( $_POST['all_short_description'] ) ) {
			update_post_meta( $post_id, 'all_short_description', wp_kses_post( wp_unslash( $_POST['all_short_description'] ) ) );
		}

		if ( isset( $_POST['all_offer_enddate'] ) ) {
			update_post_meta( $post_id, 'all_offer_enddate', sanitize_text_field( wp_unslash( $_POST['all_offer_enddate'] ) ) );
		}

		if ( isset( $_POST['all_gallery'] ) && ! empty( $_POST['all_gallery'] ) ) {
			update_post_meta( $post_id, 'all_gallery', sanitize_text_field( wp_unslash( $_POST['all_gallery'] ) ) );
		}

		if ( isset( $_POST['all_product_season'] ) ) {
			update_post_meta( $post_id, 'all_product_season', $this->recursive_sanitize_text_field( wp_unslash( $_POST['all_product_season'] ) ) ); // phpcs:ignore -- Sanitized in recursive_sanitize_text_field().
		}

		if ( isset( $_POST['all_location'] ) ) {
			update_post_meta( $post_id, 'all_product_location', $this->recursive_sanitize_text_field( wp_unslash( $_POST['all_location'] ) ) ); // phpcs:ignore -- Sanitized in recursive_sanitize_text_field().
		}

	}

	/**
	 * Recursive sanitation for an array
	 *
	 * @param array $array Array to sanitize.
	 *
	 * @return mixed
	 */
	public function recursive_sanitize_text_field( $array ) {
		foreach ( $array as $key => &$value ) {
			if ( is_array( $value ) ) {
				$value = $this->recursive_sanitize_text_field( $value );
			} else {
				$value = sanitize_text_field( $value );
			}
		}

		return $array;
	}

	/**
	 * Register all_product post type.
	 */
	public function all_product_init() {
		register_post_type(
			'all_product',
			array(
				'labels'            => array(
					'name'                  => __( 'Arctic Lakeland tuotekortit', 'all-api' ),
					'singular_name'         => __( 'Tuote', 'all-api' ),
					'all_items'             => __( 'Kaikki tuotteet', 'all-api' ),
					'attributes'            => __( 'Ominaisuudet', 'all-api' ),
					'insert_into_item'      => __( 'Lisää tuotteeseen', 'all-api' ),
					'uploaded_to_this_item' => __( 'Lisättynä tähän tuotteeseen', 'all-api' ),
					'new_item'              => __( 'Uusi tuote', 'all-api' ),
					'add_new'               => __( 'Lisää uusi', 'all-api' ),
					'add_new_item'          => __( 'Lisää uusi tuote', 'all-api' ),
					'edit_item'             => __( 'Muokkaa tuotetta', 'all-api' ),
					'view_item'             => __( 'Näytä tuote', 'all-api' ),
					'view_items'            => __( 'Näytä tuotteet', 'all-api' ),
					'featured_image'        => __( 'Tuotekuva', 'all-api' ),
					'set_featured_image'    => __( 'Aseta tuotekuva', 'all-api' ),
					'remove_featured_image' => __( 'Poista tuotekuva', 'all-api' ),
					'use_featured_image'    => __( 'Käytä tuotekuvana', 'all-api' ),
					'search_items'          => __( 'Etsi tuotetta', 'all-api' ),
					'not_found'             => __( 'Tuotteita ei löytynyt.', 'all-api' ),
					'not_found_in_trash'    => __( 'Ei tuotteita roskakorissa.', 'all-api' ),
					'menu_name'             => __( 'Tuotteet', 'all-api' ),
				),
				'public'            => false,
				'hierarchical'      => false,
				'show_ui'           => true,
				'show_in_nav_menus' => false,
				'show_in_menu'      => false,
				'supports'          => array( 'title', 'thumbnail', 'editor' ),
				'has_archive'       => false,
				'rewrite'           => true,
				'query_var'         => true,
				'show_in_rest'      => false,
			)
		);

	}

	// Register Custom Taxonomy
	public function register_all_product_cat() {

		$labels = array(
			'name'                       => _x( 'Kategoriat', 'Taxonomy General Name', 'all-api' ),
			'singular_name'              => _x( 'Kategoria', 'Taxonomy Singular Name', 'all-api' ),
			'menu_name'                  => __( 'Kategoria', 'all-api' ),
			'all_items'                  => __( 'Kaikki kategoriat', 'all-api' ),
			'parent_item'                => __( 'Ylä-kategoria', 'all-api' ),
			'parent_item_colon'          => __( 'Ylä-kategoria:', 'all-api' ),
			'new_item_name'              => __( 'Uusi kategoria', 'all-api' ),
			'add_new_item'               => __( 'Lisää uusi kategoria', 'all-api' ),
			'edit_item'                  => __( 'Muokkaa kategoriaa', 'all-api' ),
			'update_item'                => __( 'Päivitä kategoria', 'all-api' ),
			'view_item'                  => __( 'Näytä kategoria', 'all-api' ),
			'separate_items_with_commas' => __( 'Erottele kategoriat pilkulla', 'all-api' ),
			'add_or_remove_items'        => __( 'Lisää tai poista kategorioita', 'all-api' ),
			'choose_from_most_used'      => __( 'Valitse eniten käytetyistä', 'all-api' ),
			'popular_items'              => __( 'Suosituimmat kategoriat', 'all-api' ),
			'search_items'               => __( 'Hae kategorioita', 'all-api' ),
			'not_found'                  => __( 'Ei löytynyt', 'all-api' ),
			'no_terms'                   => __( 'Ei kategorioita', 'all-api' ),
		);
		$args = array(
			'labels'            => $labels,
			'hierarchical'      => true,
			'public'            => true,
			'show_ui'           => true,
			'show_admin_column' => true,
			'show_in_nav_menus' => false,
			'show_tagcloud'     => true,
		);
		register_taxonomy( 'all_product_cat', array( 'all_product' ), $args );

	}

	/**
	 * Make post type translatable with Polylang by default.
	 */
	public function add_cpt_to_pll( $post_types, $is_settings ) {

		$post_types['all_product'] = 'all_product';

		return $post_types;
	}

	/**
	 * Delete product if parent is deleted.
	 */
	public function delete_all_product( $post_id ) {

		if ( ! $post_id ) {
			return;
		}

		$args = array(
			'post_type'      => 'all_product',
			'post_status'    => 'any',
			'posts_per_page' => 1, // There should always be only one.
			'meta_query'     => array( // phpcs:ignore WordPress.DB.SlowDBQuery.slow_db_query_meta_query
				array(
					'key'     => 'all_post_parent',
					'value'   => $post_id,
					'compare' => '=',
				),
			),
		);

		$products = get_posts( $args );

		if ( $products ) {
			foreach ( $products as $p ) {
				wp_trash_post( $p->ID );
			}
		}
	}

	public function save_product_sync() {

		$id   = intval( $_GET['post_id'] );
		$sync = $_GET['sync'];

		if ( 'true' === $sync ) {
			update_post_meta( $id, 'all_product_sync', 'sync' );
			echo 'Synkronointi otettu käyttöön';
		} else {
			delete_post_meta( $id, 'all_product_sync' );
			echo 'Synkronointi poistettu käytöstä';
		}

		wp_die();

	}

	public function update_all_product( $post_id, $post, $update ) {

		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return;
		}

		// Check if not an autosave.
		if ( wp_is_post_autosave( $post_id ) ) {
			return;
		}

		// Check if not a revision.
		if ( wp_is_post_revision( $post_id ) ) {
			return;
		}

		// Check permissions
		if ( ! current_user_can( 'edit_post', $post_id ) ) {
			return;
		}

		$converter = new All_Api_Converter();
		$child     = $converter->get_existing_all_product( $post_id );
		$sync      = get_post_meta( $child[0]->ID, 'all_product_sync', true );

		if ( 'sync' !== $sync ) {
			return;
		}

		$converter->maybe_insert_new_post( $post_id, $child[0]->ID );

	}

}
