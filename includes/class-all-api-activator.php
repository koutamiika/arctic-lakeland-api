<?php

/**
 * Fired during plugin activation
 *
 * @link       https://koutamedia.fi
 * @since      0.1.0
 *
 * @package    All_Api
 * @subpackage All_Api/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      0.1.0
 * @package    All_Api
 * @subpackage All_Api/includes
 * @author     Kouta Media / Miika Salo <miika@koutamedia.fi>
 */
class All_Api_Activator {

	/**
	 * Runs on plugin activation.
	 *
	 * @since    0.1.0
	 */
	public static function activate() {

		if ( ! get_option( 'all_api_flush_rewrite_rules_option' ) ) {
			add_option( 'all_api_flush_rewrite_rules_option', true );
		}

	}

}