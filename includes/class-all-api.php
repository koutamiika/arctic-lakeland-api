<?php
/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       https://koutamedia.fi
 * @since      0.1.0
 *
 * @package    All_Api
 * @subpackage All_Api/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      0.1.0
 * @package    All_Api
 * @subpackage All_Api/includes
 * @author     Kouta Media <miika@koutamedia.fi>
 */
class All_Api {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    0.1.0
	 * @access   protected
	 * @var      All_Api_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    0.1.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    0.1.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    0.1.0
	 */
	public function __construct() {
		if ( defined( 'ALL_API_VERSION' ) ) {
			$this->version = ALL_API_VERSION;
		} else {
			$this->version = '0.1.0';
		}
		$this->plugin_name = 'all-api';

		$this->load_dependencies();
		$this->define_admin_hooks();
		$this->define_converter_hooks();
		$this->define_products_hooks();
		$this->define_api_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - All_Api_Loader. Orchestrates the hooks of the plugin.
	 * - All_Api_Admin. Defines all hooks for the admin area.
	 * - All_Api_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    0.1.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-all-api-loader.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-all-api-admin.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-all-api-converter.php';

		/**
		 * The class responsible for defining products functionality
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-all-api-products.php';

		/**
		 * All_Product class used by REST API.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-all-product.php';

		/**
		 * REST API Endpoint.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'controller/class-all-api-products-route.php';

		$this->loader = new All_Api_Loader();

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    0.1.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new All_Api_Admin( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );

		$this->loader->add_action( 'admin_menu', $plugin_admin, 'register_menu_page', 10 );
		$this->loader->add_action( 'admin_init', $plugin_admin, 'register_settings' );
		$this->loader->add_action( 'init', $plugin_admin, 'maybe_flush_rewrites', 20 );

	}

	/**
	 * Register all of the hooks related to the converter tool functionality
	 * of the plugin.
	 *
	 * @since    0.1.0
	 * @access   private
	 */
	private function define_converter_hooks() {

		$converter = new All_Api_Converter();

		if ( ! empty( $converter->options ) ) {
			$this->loader->add_filter( "bulk_actions-edit-{$converter->options['post_type']}", $converter, 'register_bulk_actions' );
			$this->loader->add_filter( "handle_bulk_actions-edit-{$converter->options['post_type']}", $converter, 'bulk_action_handler', 10, 3 );
		}
		$this->loader->add_action( 'admin_action_all_duplicate_post', $converter, 'all_duplicate_post' );
		$this->loader->add_action( 'admin_notices', $converter, 'bulk_action_admin_notice' );
		$this->loader->add_filter( 'post_row_actions', $converter, 'modify_list_row_actions', 10, 2 );
		$this->loader->add_filter( 'page_row_actions', $converter, 'modify_list_row_actions', 10, 2 );

	}

	/**
	 * Register all of the hooks related to the products functionality
	 * of the plugin.
	 *
	 * @since    0.1.0
	 * @access   private
	 */
	private function define_products_hooks() {

		$plugin_products = new All_Api_Products( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'init', $plugin_products, 'all_product_init', 10 );
		$this->loader->add_action( 'init', $plugin_products, 'register_all_product_cat', 10 );
		$this->loader->add_action( 'load-post.php', $plugin_products, 'init_metabox' );
		$this->loader->add_action( 'load-post-new.php', $plugin_products, 'init_metabox' );
		$this->loader->add_filter( 'pll_get_post_types', $plugin_products, 'add_cpt_to_pll', 10, 2 );
		$this->loader->add_action( 'wp_trash_post', $plugin_products, 'delete_all_product', 10 );
		$this->loader->add_action( 'wp_ajax_save_product_sync', $plugin_products, 'save_product_sync' );
		$this->loader->add_action( 'save_post', $plugin_products, 'update_all_product', 99, 3 );

	}

	/**
	 * Register all of the hooks related to the REST API functionality
	 * of the plugin.
	 *
	 * @since    0.1.0
	 * @access   private
	 */
	private function define_api_hooks() {

		$api = new All_Products_Route();

		$this->loader->add_action( 'rest_api_init', $api, 'register_routes' );

	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    0.1.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     0.1.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     0.1.0
	 * @return    All_Api_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     0.1.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}
