<?php
/**
 * Defines the Product object
 *
 * @package All_Api
 */

/**
 * Class All_Product
 */
class All_Product {

    /**
	 * Post ID.
	 *
	 * @since 0.1.0
	 * @var int
	 */
	public $ID;

    /**
	 * Parent post ID.
	 *
	 * @since 0.1.0
	 * @var int
	 */
	private $post_parent;

	/**
	 * The post's title.
	 *
	 * @since 0.1.0
	 * @var string
	 */
    public $title;

	/**
	 * Company name
	 *
	 * @var string
	 */
	public $company_name;

	/**
	 * Public contact email
	 *
	 * @var string
	 */
	public $email;

	/**
	 * Public contact phonenumber
	 *
	 * @var string
	 */
	public $phone;

	/**
	 * Homepage URL
	 *
	 * @var string
	 */
	public $homepage;

	/**
	 * Long description about the product / company
	 *
	 * @var string
	 */
	public $description;

	/**
	 * Short description about the product / company
	 *
	 * @var string
	 */
	public $short_description;

	/**
	 * Product price
	 *
	 * @var string
	 */
	public $price;

	/**
	 * Product price suffix
	 *
	 * @var string
	 */
	public $price_suffix;

	/**
	 * Post thumbnail
	 *
	 * @var string
	 */
	public $thumbnail;

	/**
	 * Additional photos
	 *
	 * @var array/mixed
	 */
	public $images = array();

	/**
	 * Product category
	 *
	 * @var array
	 */
	public $category;

	/**
	 * Season availability
	 *
	 * @var array
	 */
	public $season;

	/**
	 * Link to external booking provider eg. Bokún.
	 *
	 * @var string
	 */
	public $booking;

	/**
	 * Location details from Google Maps
	 *
	 * @var array/mixed
	 */
	public $location;

	/**
	 * Post created date
	 *
	 * @var string
	 */
	public $post_date;

	/**
	 * Post modified date
	 *
	 * @var string
	 */
	public $post_modified;

	/**
	 * Post language slug
	 *
	 * @var string
	 */
	public $lang;

	/**
	 * Permalink
	 *
	 * @var string
	 */
	public $url;

	/**
	 * Offer end date.
	 *
	 * @var string
	 */
	public $offer_enddate;

    /**
	 * Constructor.
	 *
	 * @since 3.5.0
	 *
	 * @param WP_Post|object $post Post object.
	 */
	public function __construct( $post ) {

		$this->ID            = absint( $post->ID );
		$this->title         = esc_html( $post->post_title );
		$this->description   = wp_kses_post( $post->post_content );
		$this->post_date     = $post->post_date;
		$this->post_modified = $post->post_modified;

		if ( function_exists( 'pll_get_post_language' ) ) {
			$this->lang = pll_get_post_language( $post->ID );
		}

		$this->set_meta( $post->ID );

	}

	public function set_meta( $post_id ) {

		$this->set_meta_company_name( $post_id );
		$this->set_meta_email( $post_id );
		$this->set_meta_phone( $post_id );
		$this->set_meta_homepage( $post_id );
		$this->set_meta_booking( $post_id );
		$this->set_meta_short_description( $post_id );
		$this->set_meta_category( $post_id );
		$this->set_meta_season( $post_id );
		$this->set_meta_location( $post_id );
		$this->set_product_thumbnail( $post_id );
		$this->set_product_photos( $post_id );
		$this->set_product_price( $post_id );
		$this->set_product_url( $post_id );
		$this->set_meta_offer_enddate( $post_id );

	}

	public function set_product_url( $post_id ) {

		$parent    = get_post_meta( $post_id, 'all_post_parent', true );
		$permalink = get_the_permalink( $parent );

		$this->url = $permalink;

	}

	public function set_product_price( $post_id ) {

		$price        = get_post_meta( $post_id, 'all_price', true );
		$price_suffix = get_post_meta( $post_id, 'all_price_suffix', true );

		$this->price        = apply_filters( 'all_meta_price', $price, $post_id );
		$this->price_suffix = $price_suffix;

	}

	public function set_meta_company_name( $post_id ) {

		$title = get_post_meta( $post_id, 'all_company_name', true );

		$this->company_name = apply_filters( 'all_meta_company_name', $title, $post_id );

	}

	public function set_meta_email( $post_id ) {

		$email = get_post_meta( $post_id, 'all_company_email', true );

		$this->email = apply_filters( 'all_meta_email', $email, $post_id );

	}

	public function set_meta_phone( $post_id ) {

		$phone = get_post_meta( $post_id, 'all_company_phone', true );

		$this->phone = apply_filters( 'all_meta_phone', $phone, $post_id );

	}

	public function set_meta_homepage( $post_id ) {

		$url = get_post_meta( $post_id, 'all_company_website', true );

		$this->homepage = apply_filters( 'all_meta_homepage', $url, $post_id );

	}

	public function set_meta_booking( $post_id ) {

		$booking = get_post_meta( $post_id, 'all_company_booking', true );

		$this->booking = apply_filters( 'all_meta_booking', $booking, $post_id );

	}

	public function set_meta_short_description( $post_id ) {

		$content = get_post_meta( $post_id, 'all_short_description', true );

		$this->short_description = apply_filters( 'all_meta_short_description', $content, $post_id );

	}

	public function set_meta_category( $post_id ) {

		$cats = get_the_terms( $post_id, 'all_product_cat' );

		$categories = array();

		if ( $cats ) {
			foreach ( $cats as $cat ) {
				$categories[] = $cat->name;
			}
		}

		$this->category = apply_filters( 'all_meta_product_cats', $categories, $post_id );

	}

	public function set_meta_season( $post_id ) {

		$cats = get_post_meta( $post_id, 'all_product_season', true );

		$this->season = apply_filters( 'all_meta_product_seasons', $cats, $post_id );

	}

	public function set_meta_location( $post_id ) {

		$loc = get_post_meta( $post_id, 'all_product_location', true );

		$this->location = apply_filters( 'all_meta_location', $loc, $post_id );

	}

	public function set_meta_offer_enddate( $post_id ) {

		$date = get_post_meta( $post_id, 'all_offer_enddate', true );

		if ( $date ) {
			$this->offer_enddate = date( 'Y-m-d', strtotime( $date ) );
		}

	}

	public function set_product_thumbnail( $post_id ) {

		$thumbnail = get_the_post_thumbnail_url( $post_id );

		if ( ! empty( $thumbnail ) ) {
			$this->thumbnail = $thumbnail;
		} else {
			$this->thumbnail = get_post_meta( $post_id, 'all_product_thumbnail', true );
		}

	}

	public function set_product_photos( $post_id ) {

		$meta = get_post_meta( $post_id, 'all_gallery', true );

		if ( ! is_array( $meta ) ) {
			$ids    = explode( ',', $meta );
			$images = array();

			foreach ( $ids as $image ) {

				$id       = absint( $image );
				$url      = wp_get_attachment_image_src( $id, 'full' );
				$images[] = $url[0];
			}

			$this->images = $images;
		} else {
			$this->images = $meta;
		}

	}

}
