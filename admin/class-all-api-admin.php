<?php
/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://koutamedia.fi
 * @since      0.1.0
 *
 * @package    All_Api
 * @subpackage All_Api/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    All_Api
 * @subpackage All_Api/admin
 * @author     Kouta Media / Miika Salo <miika@koutamedia.fi>
 */
class All_Api_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    0.1.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    0.1.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    0.1.0
	 * @param      string $plugin_name       The name of this plugin.
	 * @param      string $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version     = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    0.1.0
	 */
	public function enqueue_styles() {

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/all-api-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    0.1.0
	 */
	public function enqueue_scripts() {

		global $post;

		$id = null;

		if ( $post instanceof WP_Post ) {
			$id = $post->ID;
		}

		wp_enqueue_script( 'jquery-ui-core' );
		wp_enqueue_script( 'jquery-ui-widget' );
		wp_enqueue_script( 'jquery-ui-sortable' );

		if ( ! did_action( 'wp_enqueue_media' ) ) {
			wp_enqueue_media();
		}

		wp_enqueue_script( 'google-maps', 'https://maps.google.com/maps/api/js?sensor=false&libraries=places&key=AIzaSyDvYL6x0gD3rDyY9k5CMqpuT_pA_DlqoWg', array( 'jquery' ), $this->version, true );
		wp_enqueue_script( 'location-picker', plugin_dir_url( __FILE__ ) . 'js/location-picker.min.js', array( 'jquery' ), $this->version, true );
		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/all-api-admin.js', array( 'jquery' ), $this->version, true );
		wp_localize_script( $this->plugin_name, 'all', array(
			'location'       => get_post_meta( $id, 'all_product_location', true ),
			'current_screen' => get_current_screen(),
			'ajaxurl'        => admin_url( 'admin-ajax.php' ),
		) );

	}

	/**
	 * Register Arctic Lakeland API plugins menu
	 *
	 * @since     0.1.0
	 */
	public function register_menu_page() {
		add_menu_page(
			__( 'Arctic Lakeland', 'all-api' ),
			__( 'Arctic Lakeland', 'all-api' ),
			'publish_pages',
			'edit.php?post_type=all_product',
			'',
			'',
			2
		);

		add_submenu_page(
			'edit.php?post_type=all_product',
			'Tuotekortit',
			'Tuotekortit',
			'publish_pages',
			'edit.php?post_type=all_product'
		);

		add_submenu_page(
			'edit.php?post_type=all_product',
			'Lisää uusi tuotekortti',
			'Lisää uusi',
			'publish_pages',
			'post-new.php?post_type=all_product'
		);

		add_submenu_page(
			'edit.php?post_type=all_product',
			'Asetukset',
			'Asetukset',
			'manage_options',
			'all-settings',
			array( $this, 'load_admin_page_content' ),
		);

	}

	/**
	 * Register settings for the plugin
	 */
	public function register_settings() {
		register_setting(
			'all-api-converter-type',
			'all-api-converter-type',
		);

		register_setting(
			'all-api-settings',
			'all-api-settings',
		);

		$options = get_option( 'all-api-settings' );

		add_settings_section( 'all-api-converter-type-settings-section', 'Arctic Lakeland API Converter Settings',  array( $this, 'section_text' ), 'all-settings' );
		add_settings_field( 'all_settings_allowed_type', 'Sallittu artikkelityyppi', array( $this, 'all_settings_allowed_type' ), 'all-settings', 'all-api-converter-type-settings-section', $options );

		add_settings_section( 'all-api-settings-section', 'Synkronoitavat tiedot', array(), 'all-settings' );
		add_settings_field( 'all_settings_description', 'Pitkä kuvaus', array( $this, 'render_description_meta_select' ), 'all-settings', 'all-api-settings-section', array( $options, 'description' ) );
		add_settings_field( 'all_settings_company_name', 'Yrityksen nimi', array( $this, 'render_meta_select' ), 'all-settings', 'all-api-settings-section', array( $options, 'company_name' ) );
		add_settings_field( 'all_settings_company_phone', 'Puhelinnumero', array( $this, 'render_meta_select' ), 'all-settings', 'all-api-settings-section', array( $options, 'company_phone' ) );
		add_settings_field( 'all_settings_company_email', 'Sähköpostiosoite', array( $this, 'render_meta_select' ), 'all-settings', 'all-api-settings-section', array( $options, 'company_email' ) );
		add_settings_field( 'all_settings_company_website', 'Kotisivut', array( $this, 'render_meta_select' ), 'all-settings', 'all-api-settings-section', array( $options, 'company_website' ) );
		add_settings_field( 'all_settings_company_booking', 'Nettivaraus', array( $this, 'render_meta_select' ), 'all-settings', 'all-api-settings-section', array( $options, 'company_booking' ) );
		add_settings_field( 'all_settings_short_description', 'Lyhyt kuvaus', array( $this, 'render_meta_select' ), 'all-settings', 'all-api-settings-section', array( $options, 'short_description' ) );
		add_settings_field( 'all_settings_location', 'Sijainti', array( $this, 'render_meta_select' ), 'all-settings', 'all-api-settings-section', array( $options, 'product_location' ) );
		add_settings_field( 'all_settings_thumbnail', 'Pääkuva', array( $this, 'render_thumbnail_meta_select' ), 'all-settings', 'all-api-settings-section', array( $options, 'thumbnail' ) );
		add_settings_field( 'all_settings_gallery', 'Kuvagalleria', array( $this, 'render_meta_select' ), 'all-settings', 'all-api-settings-section', array( $options, 'gallery' ) );
		add_settings_field( 'all_settings_category', 'Kategoria', array( $this, 'all_settings_taxonomies' ), 'all-settings', 'all-api-settings-section', $options );
		add_settings_field( 'all_settings_price', 'Hinta', array( $this, 'render_meta_select' ), 'all-settings', 'all-api-settings-section', array( $options, 'price' ) );
		add_settings_field( 'all_settings_price_suffix', 'Hinnan jälkiliite', array( $this, 'render_meta_select' ), 'all-settings', 'all-api-settings-section', array( $options, 'price_suffix' ) );

	}

	public function section_text() {
		echo '<div id="message" class="notice error"><p>Varoitus! Älä muuta näitä asetuksia ellet tiedä mitä teet. Väärät asetukset saattavat johtaa virheelliseen tuotetietoon.</p></div>';
	}

	/**
	 * Get meta by post type
	 */
	public function get_meta_fields( $post_type ) {

		if ( ! $post_type ) {
			return;
		}

		$args = array(
			'post_type'      => $post_type,
			'posts_per_page' => 1,
		);

		$posts = get_posts( $args );
		$id    = $posts[0]->ID;
		$metas = get_post_custom( $id );

		return $metas;

	}

	/**
	 * Render options for allowed post type selection.
	 */
	public function all_settings_allowed_type( $options ) {
		// Get post types
		$args       = array(
			'public' => true,
		);
		$post_types = get_post_types( $args, 'objects' );
		?>

		<select class="widefat" name="all-api-settings[post_type]">
			<option value=""><?php esc_html_e( 'Valitse artikkelityyppi', 'all-api' ); ?></option>
			<?php foreach ( $post_types as $post_type_obj ) :
				$labels = get_post_type_labels( $post_type_obj );
				?>
				<option value="<?php echo esc_attr( $post_type_obj->name ); ?>" <?php selected( $options['post_type'], $post_type_obj->name ); ?>><?php echo esc_html( $labels->name ); ?></option>
			<?php endforeach; ?>
		</select>
		<?php
	}

	/**
	 * Render options for category sync.
	 */
	public function all_settings_taxonomies( $options ) {
		// Get taxonomies
		$args       = array(
			'public' => true,
		);
		$taxonomies = get_taxonomies( $args, 'objects' );
		?>

		<select class="widefat" name="all-api-settings[category]">
			<option value=""><?php esc_html_e( 'Älä synkronoi', 'all-api' ); ?></option>
			<?php foreach ( $taxonomies as $tax ) : ?>
				<option value="<?php echo esc_attr( $tax->name ); ?>" <?php selected( $options['category'], $tax->name ); ?>><?php echo esc_html( $tax->label ) . ' (' . esc_html( $tax->name ) . ')'; ?></option>
			<?php endforeach; ?>
		</select>
		<?php
	}

	/**
	 * Render options for meta selection.
	 * Gets all metafields by post type.
	 */
	public function render_meta_select( $options ) {
		$metas = $this->get_meta_fields( $options[0]['post_type'] );
		$key   = $options[0][ $options[1] ];
		?>
		<select class="widefat" name="all-api-settings[<?php echo esc_attr( $options[1] ); ?>]">
			<option value=""><?php esc_html_e( 'Älä synkronoi', 'all-api' ); ?></option>

			<?php if ( $metas ) : ?>

				<?php foreach ( $metas as $k => $v ) : ?>
					<?php if ( '_' !== substr( $k, 0, 1 ) ) : ?>
						<option value="<?php echo esc_attr( $k ); ?>" <?php selected( $options[0][ $options[1] ], $k ); ?>><?php echo esc_html( $k ); ?></option>
					<?php endif; ?>

				<?php endforeach; ?>

			<?php endif; ?>

		</select>
		<?php
	}

	/**
	 * Render options for long description sync.
	 */
	public function render_description_meta_select( $options ) {
		$metas = $this->get_meta_fields( $options[0]['post_type'] );
		$key   = $options[0][ $options[1] ];
		?>
		<select class="widefat" name="all-api-settings[<?php echo esc_attr( $options[1] ); ?>]">
			<option value=""><?php esc_html_e( 'Älä synkronoi', 'all-api' ); ?></option>
			<option value="default" <?php selected( $options[0][ $options[1] ], 'default' ); ?>><?php esc_html_e( 'Oletus (WordPress editor)', 'all-api' ); ?></option>

			<?php if ( $metas ) : ?>

				<?php foreach ( $metas as $k => $v ) : ?>
					<?php if ( '_' !== substr( $k, 0, 1 ) ) : ?>
						<option value="<?php echo esc_attr( $k ); ?>" <?php selected( $options[0][ $options[1] ], $k ); ?>><?php echo esc_html( $k ); ?></option>
					<?php endif; ?>

				<?php endforeach; ?>

			<?php endif; ?>

		</select>
		<?php
	}

	/**
	 * Render options for thumbnail sync.
	 */
	public function render_thumbnail_meta_select( $options ) {
		$metas = $this->get_meta_fields( $options[0]['post_type'] );
		$key   = $options[0][ $options[1] ];
		?>
		<select class="widefat" name="all-api-settings[<?php echo esc_attr( $options[1] ); ?>]">
			<option value=""><?php esc_html_e( 'Älä synkronoi', 'all-api' ); ?></option>
			<option value="post_thumbnail" <?php selected( $options[0][ $options[1] ], 'post_thumbnail' ); ?>><?php esc_html_e( 'Artikkelikuva', 'all-api' ); ?></option>

			<?php if ( $metas ) : ?>

				<?php foreach ( $metas as $k => $v ) : ?>
					<?php if ( '_' !== substr( $k, 0, 1 ) ) : ?>
						<option value="<?php echo esc_attr( $k ); ?>" <?php selected( $options[0][ $options[1] ], $k ); ?>><?php echo esc_html( $k ); ?></option>
					<?php endif; ?>

				<?php endforeach; ?>

			<?php endif; ?>

		</select>
		<?php
	}

	/**
	 * Load admin menu view
	 */
	public function load_admin_page_content() {
		include_once ALL_API_BASE_DIR . '/admin/partials/all-api-settings-view.php';
	}

	/**
	 * Callback for add_menu_page()
	 */
	public function all_api_menu_page_cb() {
        include_once ALL_API_BASE_DIR . '/admin/partials/all-api-admin-display.php';
	}

	/**
	 * Flush rewrite rules if the previously added flag exists,
	 * and then remove the flag.
	 *
	 * @since 0.1.0
	 */
	public function maybe_flush_rewrites() {
		if ( get_option( 'all_api_flush_rewrite_rules_option' ) ) {
			flush_rewrite_rules();
			delete_option( 'all_api_flush_rewrite_rules_option' );
		}
	}

}