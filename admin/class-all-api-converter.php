<?php
/**
 * Tool for converting post types for API
 *
 * @package all-api
 */

/**
 * ClassA ll_Api_Converter
 */
class All_Api_Converter {

	/**
	 * Plugin options
	 *
	 * @var array
	 */
	public $options;

	/**
	 * Constructor.
	 */
	public function __construct() {

		$this->options = get_option( 'all-api-settings' );

	}

	/**
	 * Add bulk action to dropdown
	 */
	public function register_bulk_actions( $bulk_actions ) {

		if ( ! current_user_can( 'publish_pages' ) ) {
			return $bulk_actions;
		}

		$bulk_actions['make_product_card'] = __( 'Luo tuotekortit', 'all-api' );

		return $bulk_actions;
	}

	/**
	 * Action handler for custom bulk edit action.
	 */
	public function bulk_action_handler( $redirect_to, $doaction, $post_ids ) {
		if ( 'make_product_card' !== $doaction ) {
			return $redirect_to;
		}

		foreach ( $post_ids as $post_id ) {

			$languages = $this->get_translations( $post_id );

			if ( $languages ) {

				$translations = array();

				/**
				 * Duplicate each translation if they exist.
				 */
				foreach ( $languages as $key => $value ) {
					$new_post = $this->maybe_insert_new_post( $value );

					$translations[ $key ] = $new_post; // $key is language slug.
				}

				/**
				 * Defines which posts are translations of each other
				 */
				if ( function_exists( 'pll_save_post_translations' ) ) {
					pll_save_post_translations( $translations );
				}
			} else {
				$new_post = $this->maybe_insert_new_post( $post_id );
			}
		}

		$redirect_to = add_query_arg( 'make_product_card', count( $post_ids ), $redirect_to );
		return $redirect_to;
	}

	/**
	 * Function creates post duplicate as a draft and redirects then to the edit post screen
	 *
	 * @param int $post_id ID of the post to duplicate.
	 */
	public function all_duplicate_post( $post_id = '' ) {

		if ( ! ( isset( $_GET['post'] ) || isset( $_POST['post'] ) || ( isset( $_REQUEST['action'] ) && 'all_duplicate_post' === $_REQUEST['action'] ) ) ) {
			wp_die( 'No post to duplicate has been supplied!' );
		}

		/**
		 * Nonce verification
		 */
		if ( ! isset( $_GET['duplicate_nonce'] ) || ! wp_verify_nonce( wp_unslash( esc_html( $_GET['duplicate_nonce'] ) ), 'duplicate_nonce_action' ) ) {
			return;
		}

		/**
		 * Get the original post id
		 */
		$post_id = ( isset( $_GET['post'] ) ? absint( $_GET['post'] ) : absint( $_POST['post'] ) );

		$languages = $this->get_translations( $post_id );

		if ( $languages ) {

			$translations = array();

			/**
			 * Duplicate each translation if they exist.
			 */
			foreach ( $languages as $key => $value ) {
				$new_post = $this->maybe_insert_new_post( $value );

				$translations[ $key ] = $new_post; // $key is language slug.
			}

			/**
			 * Defines which posts are translations of each other
			 */
			if ( function_exists( 'pll_save_post_translations' ) ) {
				pll_save_post_translations( $translations );
			}
		} else {
			$new_post = $this->maybe_insert_new_post( $post_id );
		}

		/**
		 * Redirect to newly created post.
		 */
		wp_safe_redirect( admin_url( 'post.php?action=edit&created=created&post=' . $new_post ) );
		exit;

	}

	/**
	 * Get post's all Polylang translations.
	 */
	public function get_translations( $post_id ) {

		if ( ! $post_id ) {
			return;
		}

		if ( ! function_exists( 'pll_get_post_translations' ) ) {
			return;
		}

		$translations = pll_get_post_translations( $post_id );

		return $translations;

	}

	public function get_term_translations( $term_id ) {

		if ( ! $term_id ) {
			return;
		}

		if ( ! function_exists( 'pll_get_term_translations' ) ) {
			return;
		}

		$translations = pll_get_term_translations( $term_id );

		return $translations;

	}

	/**
	 * Create a new post
	 *
	 * @param int $post_id ID of the post to duplicate.
	 * @param int $update ID of the post to update.
	 */
	public function maybe_insert_new_post( $post_id = '', $update = '' ) {

		$existing = $this->get_existing_all_product( $post_id );

		if ( $existing && empty( $update ) ) {
			return;
		}

		/**
		 * Get original post
		 */
		$post = get_post( $post_id );

		/**
		 * If post data exists, create the post duplicate
		 */
		if ( isset( $post ) && null !== $post ) {

			/**
			 * New post data array
			 */
			$args = array(
				'post_author'  => $post->post_author,
				'post_content' => $post->post_content,
				'post_name'    => $post->post_name,
				'post_status'  => $post->post_status,
				'post_title'   => $post->post_title,
				'post_type'    => 'all_product',
				'meta_input'   => array(
					'all_post_parent'  => absint( $post->ID ),
				),
			);

			/**
			 * Update post instead of creating a new one.
			 */
			if ( ! empty( $update ) ) {
				$args['ID'] = $update;
			}

			/**
			 * Activate sync by default when creating a new post.
			 */
			if ( empty( $update ) ) {
				$args['meta_input']['all_product_sync'] = 'sync';
			}

			$cats = array();

			// Save meta from options.
			foreach ( $this->options as $key => $value ) {

				/**
				 * Don't save these as meta values.
				 */
				if ( empty( $value ) || 'thumbnail' === $key || 'default' === $key || 'post_type' === $key ) {
					continue;
				}

				$lang = '';

				/**
				 * Get the language of the original post.
				 */
				if ( function_exists( 'pll_get_post_language' ) ) {
					$lang = pll_get_post_language( $post->ID, 'slug' );
				}

				$meta = get_post_meta( $post->ID, $value, true );

				if ( 'gallery' === $key ) {

					if ( ! is_array( $meta ) ) {
						continue;
					}

					$args['meta_input']['all_gallery'] = implode( ',', $meta );

				} elseif ( 'product_location' === $key ) {

					if ( ! is_array( $meta ) ) {
						continue;
					}

					/**
					 * Assumes ACF Google Map -field
					 */
					$args['meta_input']['all_product_location'] = array(
						'street' => $meta['address'],
						'lat'    => $meta['lat'],
						'lon'    => $meta['lng'],
					);
				} elseif ( 'category' === $key ) {

					// Get terms from selected taxonomy.
					$terms = get_the_terms( $post->ID, $value );

					if ( $terms ) {
						foreach ( $terms as $term ) {
							$cats[] = $term;
						}
					}
				} else {

					$args['meta_input'][ 'all_' . $key ] = wp_strip_all_tags( $meta, true );

				}
			}

			if ( 'default' !== $this->options['description'] ) {

				$description          = get_post_meta( $post->ID, $this->options['description'], true );
				$args['post_content'] = wp_kses_post( $description );
			}

			$args = apply_filters( 'all_insert_post_args', $args, $post );

			/**
			 * Insert the post by wp_insert_post() function
			 */
			$new_post_id = wp_insert_post( $args );

			if ( ! is_wp_error( $new_post_id ) ) {

				/**
				 * Set the new post's language the same as the original one.
				 */
				if ( function_exists( 'pll_set_post_language' ) ) {
					pll_set_post_language( $new_post_id, $lang );
				}

				/**
				 * Set post thumbnail
				 */
				if ( 'post_thumbnail' === $this->options['thumbnail'] ) {

					$thumbnail = get_post_thumbnail_id( $post );

					if ( $thumbnail ) {
						set_post_thumbnail( $new_post_id, $thumbnail );
					}
				} else {

					$thumbnail = get_post_meta( $post->ID, $this->options['thumbnail'], true );

					/**
					 * Assuming we're using ACF Gallery -field.
					 */
					if ( is_array( $thumbnail ) ) {
						set_post_thumbnail( $new_post_id, absint( $thumbnail['id'] ) );
					} else {
						set_post_thumbnail( $new_post_id, absint( $thumbnail ) );
					}
				}

				do_action( 'before_save_categories', $post, $new_post_id, $cats );

				if ( $cats ) {
					foreach ( $cats as $cat ) {
						$terms = $this->get_term_translations( $cat->term_id );

						if ( $terms ) {

							$term_translations = array();

							/**
							 * Duplicate each translation if they exist.
							 */
							foreach ( $terms as $key => $value ) {
								wp_set_object_terms( $new_post_id, $cat->name, 'all_product_cat', true );
								$term_translations[ $key ] = $term; // $key is language slug.
							}

						}
					}

				}

				do_action( 'all_after_insert_post', $new_post_id );

				return $new_post_id;
			}
		} else {

			wp_die( 'Post creation failed, could not find original post: ' . esc_html( $post_id ) );

		}

		return $post_id;
	}

	/**
	 * Currently unused.
	 */
	public function set_product_category( $post_id, $slug, $lang ) {

		wp_set_object_terms( $post_id, $slug, 'all_product_cat' );

		$term = get_term_by( 'slug', $slug, 'all_product_cat' );

		pll_set_term_language( $term->term_id, $lang );

		return $term->term_id;

	}

	/**
	 * Get Arctic Lakelands products by parent ID.
	 * Used to prevent duplicates and enabling data sync.
	 *
	 * @param int $post_id Original post ID that was used to create Arctic Lakeland product card.
	 */
	public static function get_existing_all_product( $post_id ) {

		if ( ! $post_id ) {
			return;
		}

		$args = array(
			'post_type'      => 'all_product',
			'post_status'    => 'any',
			'posts_per_page' => 1, // There should always be only one.
			'meta_query'     => array( // phpcs:ignore WordPress.DB.SlowDBQuery.slow_db_query_meta_query
				array(
					'key'     => 'all_post_parent',
					'value'   => $post_id,
					'compare' => '=',
				),
			),
		);

		$child = get_posts( $args );

		if ( $child ) {
			return $child;
		} else {
			return false;
		}

	}

	/**
	 * Add row action link
	 *
	 * @param array  $actions An array of row action links.
	 * @param object $post The post object.
	 */
	public function modify_list_row_actions( $actions, $post ) {

		if ( ! current_user_can( 'publish_pages' ) ) {
			return $actions;
		}

		$post_types = apply_filters( 'all_allowed_post_types', $this->options['post_type'] );

		if ( is_array( $post_types ) ) {
			foreach ( $post_types as $pt ) {
				if ( ! in_array( $post->post_type, $post_types, true ) ) {
					return $actions;
				}
			}
		} else {
			if ( $this->options['post_type'] !== $post->post_type ) {
				return $actions;
			}
		}

		$existing = $this->get_existing_all_product( $post->ID );

		// Add action link only if product card hasn't been made before.
		if ( ! $existing ) {
			$actions['all_duplicate'] = '<a href="' . wp_nonce_url( 'admin.php?action=all_duplicate_post&post=' . $post->ID, 'duplicate_nonce_action', 'duplicate_nonce' ) . '" title="' . esc_html__( 'Luo Arctic Lakeland tuotekortti', 'all-api' ) . '" rel="permalink">' . esc_html__( 'Luo Arctic Lakeland tuotekortti', 'all-api' ) . '</a>';
		}

		return $actions;
	}

	/**
	 * Add admin notices for actions
	 */
	public function bulk_action_admin_notice() {
		if ( ! empty( $_REQUEST['make_product_card'] ) ) {
			$count = intval( $_REQUEST['make_product_card'] );
			printf( '<div id="message" class="notice updated is-dismissible"><p>' .	esc_html( _n( '%s tuotekortti luotu.', '%s tuotekorttia luotu.', $count, 'all-api' ) ) . '</p></div>', $count );
		}

		if ( isset( $_REQUEST['created'] ) && ! empty( $_REQUEST['created'] ) ) {
			echo '<div id="message" class="notice updated is-dismissible"><p>' . esc_html__( 'Tuotekortti luotu onnistuneesti. Tarkista sisältö ja täytä mahdolliset puuttuvat kentät.', 'all-api' ) . '</p></div>';
		}
	}

}