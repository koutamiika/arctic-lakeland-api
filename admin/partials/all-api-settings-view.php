<?php
/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://koutamedia.fi
 * @since      0.1.0
 * @package all-api
 */

?>

<div class="wrap">

	<form method="post" action="options.php">

		<?php settings_fields( 'all-api-settings' ); ?>
		<?php do_settings_sections( 'all-settings' ); ?>

		<?php submit_button(); ?>

	</form>

</div>
