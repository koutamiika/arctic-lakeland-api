<?php
/**
 * Company info metafields
 *
 * @link       https://koutamedia.fi
 * @since      0.1.0
 *
 * @package    All_Api
 * @subpackage All_Api/admin/partials
 */

?>

<p>Lisää yrityksen yhteystiedot.</p> <?php // Update or remove me. ?>

<table class="form-table">

	<tbody>
		<tr>
			<th><label for="all_company_name" class="all-label"><?php esc_html_e( 'Yrityksen nimi (Vaadittu)', 'all-api' ); ?></label></th>
			<td><input class="widefat" type="text" id="all_company_name" name="all_company_name" required="required" value="<?php echo esc_attr( get_post_meta( $post->ID, 'all_company_name', true ) ); ?>" /></td>
		</tr>
		<tr>
			<th><label for="all_company_phone" class="all-label"><?php esc_html_e( 'Puhelinnumero', 'all-api' ); ?></label></th>
			<td><input class="widefat" type="text" id="all_company_phone" name="all_company_phone" value="<?php echo esc_attr( get_post_meta( $post->ID, 'all_company_phone', true ) ); ?>" /></td>
		</tr>
		<tr>
			<th><label for="all_company_email" class="all-label"><?php esc_html_e( 'Sähköpostiosoite (Vaadittu)', 'all-api' ); ?></label></th>
			<td><input class="widefat" type="email" id="all_company_email" name="all_company_email" required="required" value="<?php echo esc_attr( get_post_meta( $post->ID, 'all_company_email', true ) ); ?>" /></td>
		</tr>
		<tr>
			<th><label for="all_company_website" class="all-label"><?php esc_html_e( 'Kotisivut', 'all-api' ); ?></label></th>
			<td><input class="widefat" type="url" id="all_company_website" name="all_company_website" value="<?php echo esc_attr( get_post_meta( $post->ID, 'all_company_website', true ) ); ?>" placeholder="https://"/></td>
		</tr>
		<tr>
			<th><label for="all_company_booking" class="all-label"><?php esc_html_e( 'Nettivaraus', 'all-api' ); ?></label></th>
			<td><input class="widefat" type="url" id="all_company_booking" name="all_company_booking" value="<?php echo esc_attr( get_post_meta( $post->ID, 'all_company_booking', true ) ); ?>" placeholder="https://"/></td>
		</tr>
	</tbody>

</table>

<hr>

<p>Tuotteen tiedot</p>

<table class="form-table">

	<tbody>
		<tr>
			<th>
				<label for="all_product_price" class="all-label"><?php esc_html_e( 'Hinta alk.', 'all-api' ); ?></label>
			</th>
			<td>
				<input class="widefat" type="number" id="all_product_price" name="all_product_price" value="<?php echo esc_attr( get_post_meta( $post->ID, 'all_price', true ) ); ?>" />
				<p class="description"><?php esc_html_e( 'Hinta alkaen / henkilö. Kirjoita kenttään pelkästään euromääräinen summa. Euro-merkki tulostuu hinnan perään automaattisesti.', 'all-api'); ?></p>
			</td>
		</tr>
		<tr>
			<th>
				<label for="all_price_suffix" class="all-label"><?php esc_html_e( 'Hinnan jälkiliite', 'all-api' ); ?></label>
			</th>
			<td>
				<input class="widefat" type="text" id="all_price_suffix" name="all_price_suffix" value="<?php echo esc_html( get_post_meta( $post->ID, 'all_price_suffix', true ) ); ?>" />
				<p class="description"><?php esc_html_e( 'Teksti, joka näytetään hinnan jälkeen. Esim. / 2vrk / 2hlö tai per hlö', 'all-api'); ?></p>
			</td>
		</tr>
		<tr>
			<th>
				<label for="all_offer_enddate" class="all-label"><?php esc_html_e( 'Tarjous päättyy', 'all-api' ); ?></label>
			</th>
			<td>
				<input class="widefat" type="date" id="all_offer_enddate" name="all_offer_enddate" value="<?php echo esc_attr( get_post_meta( $post->ID, 'all_offer_enddate', true ) ); ?>" />
				<p class="description"><?php esc_html_e( 'Tarjouksen tai kampanjan päättymispäivä.', 'all-api'); ?></p>
			</td>
		</tr>
	</tbody>

</table>