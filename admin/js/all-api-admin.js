/*
 * A custom function that checks if element is in array, we'll need it later
 */
function in_array(el, arr) {
	for(var i in arr) {
		if(arr[i] == el) return true;
	}
	return false;
}

jQuery( function( $ ) {

	$('#all-product-sync-checkbox').change(function(e) {
        e.preventDefault();
		var id_post = $(this).attr('data-id');
		var checked = $(this).prop('checked');
		console.log(checked);
        $.ajax({
			type: 'GET',
			url: all.ajaxurl,
			data: {
				'post_id': id_post,
				'sync': checked,
				'action': 'save_product_sync',
			},
            success: function(result) {
                if ( checked ) {
					$('.messages').html('<p>Synkronointi otettu käyttöön.</p>');
				} else {
					$('.messages').html('<p>Synkronointi poistettu käytöstä.</p>');
				}
            }
        });
    });

	$('#all-map').locationpicker({
		location: {
			longitude: ( all.location.lon ? all.location.lon : '27.7278497' ),
			latitude: ( all.location.lat ? all.location.lat : '64.2221775' ),
		},
		locationName: '',
		enableAutocomplete: true,
		enableReverseGeocode: true,
		radius: 100,
		scrollwheel: false,
		addressFormat: 'street_address',
		inputBinding: {
			locationNameInput: $('#address')
		},
		oninitialized: function() {

			if ( all.location.street !== '' ) {
				$('#address').val(all.location.street);
			} else {
				$('#address').val('');
			}

		},
		onchanged: function(currentLocation) {
			$('#lat').val(currentLocation.latitude);
			$('#lon').val(currentLocation.longitude);
		}
	});

	/*
	 * Sortable images
	 */
	$('ul.all_gallery_list').sortable({
		items:'li',
		cursor:'-webkit-grabbing', /* mouse cursor */
		scrollSensitivity:40,
		start:function(event,ui){
			ui.item.css({'background-color':'grey'});
		},
		stop:function(event,ui){
			ui.item.removeAttr('style');

			var sort = new Array(), /* array of image IDs */
			    gallery = $(this); /* ul.all_gallery_mtb */

			/* each time after dragging we resort our array */
			gallery.find('li').each(function(index){
				sort.push( $(this).attr('data-id') );
			});
			/* add the array value to the hidden input field */
			gallery.parent().next().val( sort.join() );
			/* console.log(sort); */
		}
	});
	/*
	 * Multiple images uploader
	 */
	$('.all_upload_gallery_button').click( function(e){ /* on button click*/
		e.preventDefault();

		var button = $(this),
		    hiddenfield = button.prev(),
		    hiddenfieldvalue = hiddenfield.val().split(","), /* the array of added image IDs */
	    	custom_uploader = wp.media({
				title: 'Syötä kuvia galleriaan', /* popup title */
				library : {type : 'image'},
				button: {text: 'Käytä valittuja kuvia'}, /* "Insert" button text */
				multiple: true
		    }).on('select', function() {

			var attachments = custom_uploader.state().get('selection').map(function( a ) {
				a.toJSON();
            	return a;
			}),
			thesamepicture = false,
			i;

			/* loop through all the images */
          		for (i = 0; i < attachments.length; ++i) {

				/* if you don't want the same images to be added multiple time */
				if( !in_array( attachments[i].id, hiddenfieldvalue ) ) {

					/* add HTML element with an image */
					$('ul.all_gallery_list').append('<li data-id="' + attachments[i].id + '"><span style="background-image:url(' + attachments[i].attributes.url + ')"></span><div class="actions"><a href="#" class="all_gallery_remove">×</a></div></li>');
					/* add an image ID to the array of all images */
					hiddenfieldvalue.push( attachments[i].id );
				} else {
					thesamepicture = true;
				}
          		}
			/* refresh sortable */
			$( "ul.all_gallery_list" ).sortable( "refresh" );
			/* add the IDs to the hidden field value */
			hiddenfield.val( hiddenfieldvalue.join() );
			/* you can print a message for users if you want to let you know about the same images */
			if( thesamepicture == true ) alert('Kuva on jo galleriassa. Et voi lisätä sitä uudestaan.');
		}).open();
	});

	/*
	 * Remove certain images
	 */
	$('body').on('click', '.all_gallery_remove', function() {
		var id = $(this).parent().parent().attr('data-id'),
		    gallery = $(this).parent().parent().parent(),
		    hiddenfield = gallery.parent().next(),
		    hiddenfieldvalue = hiddenfield.val().split(","),
		    i = hiddenfieldvalue.indexOf(id);

			console.log(gallery)

		$(this).parent().parent().remove();

		/* remove certain array element */
		if(i != -1) {
			hiddenfieldvalue.splice(i, 1);
		}

		/* add the IDs to the hidden field value */
		hiddenfield.val( hiddenfieldvalue.join() );

		/* refresh sortable */
		gallery.sortable( "refresh" );

		return false;
	});
	/*
	 * Selected item
	 */
	$('body').on('mousedown', 'ul.all_gallery_list li', function(){
		var el = $(this);
		el.parent().find('li').removeClass('all-active');
		el.addClass('all-active');
	});
});