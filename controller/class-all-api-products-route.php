<?php
/**
 * Products route controller class.
 * Provides our REST API endpoint
 *
 * @package all-api
 */

/**
 * Class All_Products_Route
 */
class All_Products_Route {

	/**
	 * Register the routes for the objects of the controller.
	 */
	public function register_routes() {
		$version   = '1';
		$namespace = 'all/v' . $version;
		$base      = 'products';

		register_rest_route(
			$namespace,
			'/' . $base,
			array(
				array(
					'methods'             => WP_REST_Server::READABLE,
					'callback'            => array( $this, 'get_items' ),
					'permission_callback' => array( $this, 'get_items_permissions_check' ),
					'args'                => array(),
				),
			)
		);

		register_rest_route(
			$namespace,
			'/category/(?P<slug>\S+)',
			array(
				array(
					'methods'             => WP_REST_Server::READABLE,
					'callback'            => array( $this, 'get_category_items' ),
					'permission_callback' => array( $this, 'get_items_permissions_check' ),
					'args'                => array(
						'context' => array(
							'default' => 'view',
						),
					),
				),
			)
		);

		register_rest_route(
			$namespace,
			'/' . $base . '/(?P<id>[\d]+)',
			array(
				array(
					'methods'             => WP_REST_Server::READABLE,
					'callback'            => array( $this, 'get_item' ),
					'permission_callback' => array( $this, 'get_item_permissions_check' ),
					'args'                => array(
						'context' => array(
							'default' => 'view',
						),
					),
				),
			)
		);
	}

	/**
	 * Get a collection of items
	 *
	 * @param WP_REST_Request $request Full data about the request.
	 * @return WP_Error|WP_REST_Response
	 */
	public function get_items( $request ) {
		$params = $request->get_query_params();

		$args = array(
			'post_type'      => 'all_product',
			'post_status'    => 'publish',
			'posts_per_page' => $params['per_page'],
			'order'          => $params['order'],
			'orderby'        => $params['orderby'],
			'lang'           => $params['lang'], // polylang language slug
		);

		if ( $params['page'] ) {
			$args['paged'] = $params['page'];
		}

		if ( $params['search'] ) {
			$args['s'] = $params['search'];
		}

		$query = new WP_Query( $args );
		$items = $query->get_posts();
		$data  = array();

		foreach ( $items as $item ) {
			$itemdata = $this->prepare_item_for_response( $item, $request );
			$data[]   = $this->prepare_response_for_collection( $itemdata );
		}

		$total_posts = $query->found_posts;
		$max_pages   = $query->max_num_pages;
		$response    = new WP_REST_Response( $data, 200 );
		$response->header( 'X-WP-Total', $total_posts );
		$response->header( 'X-WP-TotalPages', $max_pages );

		return $response;
	}

	/**
	 * Get a collection of category items
	 *
	 * @param WP_REST_Request $request Full data about the request.
	 * @return WP_Error|WP_REST_Response
	 */
	public function get_category_items( $request ) {
		$params = $request->get_params();

		$args = array(
			'post_type'      => 'all_product',
			'post_status'    => 'publish',
			'posts_per_page' => $params['per_page'],
			'order'          => $params['order'],
			'orderby'        => $params['orderby'],
			'lang'           => $params['lang'], // polylang language slug
			'meta_query'     => array( // phpcs:ignore WordPress.DB.SlowDBQuery.slow_db_query_meta_query
				array(
					'key'     => 'all_product_cat',
					'value'   => $params['slug'],
					'compare' => 'LIKE',
				),
			),
		);

		if ( $params['page'] ) {
			$args['paged'] = $params['page'];
		}

		if ( $params['search'] ) {
			$args['s'] = $params['search'];
		}

		$query = new WP_Query( $args );
		$items = $query->get_posts();
		$data  = array();

		foreach ( $items as $item ) {
			$itemdata = $this->prepare_item_for_response( $item, $request );
			$data[]   = $this->prepare_response_for_collection( $itemdata );
		}

		$total_posts = $query->found_posts;
		$max_pages   = $query->max_num_pages;
		$response    = new WP_REST_Response( $data, 200 );
		$response->header( 'X-WP-Total', $total_posts );
		$response->header( 'X-WP-TotalPages', $max_pages );

		return $response;
	}

	/**
	 * Get one item from the collection
	 *
	 * @param WP_REST_Request $request Full data about the request.
	 * @return WP_Error|WP_REST_Response
	 */
	public function get_item( $request ) {
		// get parameters from request.
		$params = $request->get_params();

		$item = get_post( $params['id'] );
		$data = $this->prepare_item_for_response( $item, $request );

		// return a response or error based on some conditional.
		if ( $data ) {
			return new WP_REST_Response( $data, 200 );
		} else {
			return new WP_Error( 'code', __( 'Something went wrong', 'all-api' ) );
		}
	}

	/**
	 * Check if a given request has access to get items
	 *
	 * @param WP_REST_Request $request Full data about the request.
	 * @return WP_Error|bool
	 */
	public function get_items_permissions_check( $request ) {
		return true;
	}

	/**
	 * Check if a given request has access to get a specific item
	 *
	 * @param WP_REST_Request $request Full data about the request.
	 * @return WP_Error|bool
	 */
	public function get_item_permissions_check( $request ) {
		return $this->get_items_permissions_check( $request );
	}

	/**
	 * Prepare the item for the REST response
	 *
	 * @param mixed           $item WordPress representation of the item.
	 * @param WP_REST_Request $request Request object.
	 * @return mixed
	 */
	public function prepare_item_for_response( $item, $request ) {
		return new All_Product( $item );
	}

	public function prepare_response_for_collection( $response ) {
		if ( ! ( $response instanceof WP_REST_Response ) ) {
			return $response;
		}

		$data   = (array) $response->get_data();
		$server = rest_get_server();
		$links  = $server::get_compact_response_links( $response );

		if ( ! empty( $links ) ) {
			$data['_links'] = $links;
		}

		return $data;
	}

	/**
	 * Get the query params for collections
	 *
	 * @return array
	 */
	public function get_collection_params() {
		return array(
			'page'     => array(
				'description'       => 'Current page of the collection.',
				'type'              => 'integer',
				'default'           => 1,
				'sanitize_callback' => 'absint',
			),
			'per_page' => array(
				'description'       => 'Maximum number of items to be returned in result set.',
				'type'              => 'integer',
				'default'           => 10,
				'sanitize_callback' => 'absint',
			),
			'search'   => array(
				'description'       => 'Limit results to those matching a string.',
				'type'              => 'string',
				'sanitize_callback' => 'sanitize_text_field',
			),
		);
	}
}