=== Arctic Lakeland API ===
Contributors: Miika Salo
Requires at least: 5.0
Tested up to: 5.7
Stable tag: 1.0.1
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Provides REST API endpoint for Arctic Lakeland

== Changelog ==

= 1.0.1 =
* Fix price_suffix meta saving.

= 1.0 =
* Added price_suffix meta.
* Added offer_enddate meta.
* Added all_allowed_post_types filter.
* Removed site specific functions.

= 0.9.2 =
* Return if existing post has been found instead of wp_die.

= 0.9.1 =
* Sync product status from parent products when synchronizing data.
* Get price from Bokun API.
* Renamed default editors option value to avoid possible conflicts.

= 0.9.0 =
* Combine more info to description (Paljakka and Wild Taiga).
* Added Hinta -option and metafield.
* Added parent product url to API.

= 0.8.1 =
* Added product season (Paljakka).

= 0.8.0 =
* Added product category sync.

= 0.7.0 =
* Added product season (Wild Taiga).

= 0.6.0 =
* Update Doerz and Bokun products with automatically once a day (Wild Taiga).

= 0.5.0 =
* Allow content synchronization between posts

= 0.4.1 =
* Added X-WP-Total and X-WP-TotalPages headers

= 0.4.0 =
* Wildtaiga integration

= 0.3.0 =
* Small integration refactoring

= 0.2.0 =
* Converter tool alpha
